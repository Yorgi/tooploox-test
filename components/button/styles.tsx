import css from 'styled-jsx/css';

export const styles = css`
  .Button {
    background-color: #452CDC;
    color: #FFF;
    border-radius: 8px;
    padding: 10px 24px;
  }
`;