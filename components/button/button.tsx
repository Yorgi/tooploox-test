import { styles } from "./styles";

export const Button: React.FunctionComponent<Props> = ({children, ...buttonProps}) => (
  <button {...buttonProps} className="Button">
    {children}
    <style jsx>{styles}</style>
  </button>
)

type Props = React.ComponentProps<'button'>;