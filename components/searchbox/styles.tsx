import css from "styled-jsx/css";

export const styles = css`
  .SearchBox {
    display: flex;
    gap: 1rem;
    height: 36px;
    padding: 2rem 0.5rem;
    box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.05),
      0px 1px 10px rgba(0, 0, 0, 0.05);
    margin: 0 -0.5rem;
  }

  .SearchBox > input {
    flex: 1;
    border-radius: 10px;
    padding: 0.625rem 0.75rem;
    background-color: #f3f3f3;
    border: none;
  }
`;
