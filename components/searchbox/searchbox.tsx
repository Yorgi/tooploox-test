import React from "react";
import { styles } from "./styles";
import { Button } from "../button";
import { useRouter } from "next/router";

export const SearchBox: React.FunctionComponent<Props> = ({ onSearch }) => {
  const router = useRouter();
  const [filter, setFilter] = React.useState<string>("");
  const doSearch = () => {
    router.replace('/');
    onSearch(filter);
  }
  const onEnterPressed = (event: React.KeyboardEvent) => {
    if(event.key === 'Enter' ) {
      onSearch(filter);
    }
  }

  return (
    <div className="SearchBox">
      <input
        placeholder="Search github users"
        onChange={(event) => setFilter(event.target.value)}
        onKeyPress={onEnterPressed}
      />
      <Button onClick={doSearch}>Search</Button>
      <style jsx>{styles}</style>
    </div>
  );
};

type Props = {
  onSearch(filter: string): void;
};
