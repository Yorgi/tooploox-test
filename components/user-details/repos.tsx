import React from "react";
import { useGithubUserRepositories } from "../../data/hooks";
import { GithubRepo } from "../../data/models";
import { List, NoResultFound } from "../list";

export const UserRepositories: React.FunctionComponent<Props> = ({
  username,
}) => {
  const [repositories, isLoading, isError] =
    useGithubUserRepositories(username);

  let content: React.ReactChild = null;

  if (isError) {
    content = "Unable to fetch repositories: error occured";
  } else if (isLoading) {
    content = "User details is loading...";
  } else if (repositories.length === 0) {
    content = <NoResultFound>This github user has no repository</NoResultFound>;
  } else {
    content = (
      <List<GithubRepo>
        items={repositories.slice(0, 3)}
        displayProp="name"
        getKey={(item) => item.id}
        getHref={(item) => item.url}
        startAdornment={startAdornment}
      />
    );
  }

  return (
    <section>
      <h4>Top repositories</h4>
      {content}
    </section>
  );
};

const startAdornment: React.ReactElement = (
  <img src="/book-library-shelf.png" alt="repository adornment" />
);

type Props = {
  username: string;
};
