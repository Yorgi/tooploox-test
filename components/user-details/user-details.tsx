import type { GithubUser } from "../../data/models";
import { UserRepositories } from "./repos";
import { styles } from "./styles";

export const UserDetails: React.FunctionComponent<Props> = ({ user }) => {

  return (
    <div>
       <section className="UserDetails-Avatar">
        <img className="UserDetails-Avatar__img" src={user.avatar_url} alt="Avatar" />
        <p>{user.name || user.login}</p>
      </section>
      <section className="UserDetails-About">
        <h4>About</h4>
        <p className="UserDetails-About__text">{user.bio || '---'}</p>
      </section>
      <UserRepositories username={user.login} />
      <style jsx>{styles}</style>
    </div>
  );
};

type Props = {
  user: GithubUser;
};
