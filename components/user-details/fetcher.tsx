import React from "react";
import { useAppState } from "../../contexts/app-state";
import { useGithubUserDetails } from "../../data/hooks";

export const UserDetailsFetcher: React.FunctionComponent<Props> = ({ login }) => {
  const { dispatch } = useAppState();
  const [user, isLoading, isError] = useGithubUserDetails(login);

  React.useEffect(() => {
    if (user) {
      dispatch({ type: "SELECT_USER", payload: user });
    }
  }, [user, dispatch]);

  if (isError) {
    return <React.Fragment>Unable to fetch user details: error occured.</React.Fragment>;
  }

  if (isLoading) {
    return <React.Fragment>User details is loading...</React.Fragment>;
  }

  return null;
};

type Props = {
  login: string;
};
