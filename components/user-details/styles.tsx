import css from 'styled-jsx/css';

export const styles = css`
  .UserDetails-Avatar {
    display: flex;
    flex-direction: column;
    align-items: center;
    padding-top: 2rem;
  }

  .UserDetails-Avatar__img {
    max-height: 120px;
    object-fit: contain;
    object-position: center;
    border-radius: 50%;
  }

  .UserDetails-About__text {
    color: #7D838B;
    font-size: 0.8125rem;
    line-height: 140%;
  }
`;