import css from "styled-jsx/css";

export const styles = css`
  .List {
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    padding: 0px;
    gap: 0.25rem;
  }

  .List-Item {
    display: flex;
    width: 100%;
    height: 47px;
    padding: 0.875rem 1rem;
    background: #f7f7f7;
    border-radius: 6px;
    box-sizing: border-box;
    font-weight: 500;
    font-size: 0.9375rem;
    gap: 0.5rem;
  }

  .List-Item__text {
    flex: 2;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
