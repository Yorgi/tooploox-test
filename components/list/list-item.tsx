import Link from "next/link";
import { styles } from "./styles";

export const ListItem: React.FunctionComponent<Props> = ({ startAdornment, item, displayProp, key, href, onSelect }) => (
  <Link href={href}>
    <li className="List-Item" key={key} onClick={onSelect && (() => onSelect(item))}>
      {startAdornment}
      <span className="List-Item__text" >{item[displayProp]}</span>
      <img src="/chevron.png" alt="Chevron right" />
      <style jsx>{styles}</style>
    </li>
  </Link>
);

type Props = {
  item: unknown;
  displayProp: string | number | symbol;
  key: React.Key;
  href: string;
  startAdornment?: React.ReactElement
  onSelect?(item: unknown): void;
};

