export const NoResultFound: React.FunctionComponent = ({ children }) => {
  return (
    <div className="Container">
      <p>{children}</p>
      <img src="/no-result.png" alt="No result" />
      <style jsx>{`
        .Container {
          display: flex;
          align-items: center;
          flex-direction: column;
        }

        .Container > p {
          font-weight: 500;
          font-size: 17px;
          line-height: 130%;
        }

        .Container > img {
          object-fit: contain;
          object-position: center;
          width: 100%;
          max-width: 300px;
        }
      `}</style>
    </div>
  );
};
