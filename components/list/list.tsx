import React from "react";
import { ListItem } from "./list-item";
import { NoResultFound } from "./no-result";
import { styles } from "./styles";

export const List = <T extends {}>({
  items,
  children,
  getHref,
  getKey,
  ...listItemProps
}: Props<T>) => {
  return (
    <ul className="List">
      {items.map((item) => (
        <ListItem
          key={getKey(item)}
          href={getHref(item)}
          item={item}
          {...listItemProps}
        />
      ))}
      <style jsx>{styles}</style>
    </ul>
  );
};

type Props<T> = React.PropsWithChildren<{
  items: T[];
  displayProp: keyof T;
  startAdornment?: React.ReactElement;
  onSelect?(item: T): void;
  getKey(item: T): React.Key;
  getHref(item: T): string;
}>;
