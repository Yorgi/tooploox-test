import { request } from "@octokit/request";
import type { Endpoints, Su } from "@octokit/types";

declare type GithubSimpleUser = Unwrap<Endpoints["GET /search/users"]>["items"][0];
declare type GithubUser = Unwrap<Endpoints["GET /users/{username}"]>;
declare type GithubRepo = Unwrap<Endpoints["GET /users/{username}/repos"]>[0];

type Unwrap<T> = T['response']['data'];