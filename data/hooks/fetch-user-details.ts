import React from "react";
import { request } from "@octokit/request";
import { GithubUser } from "../models";
import { ApiHookResult } from ".";

export function useGithubUserDetails(
  username: string
): useGithubUserDetailsResult {
  const [user, setUser] = React.useState<GithubUser>();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [isError, setIsError] = React.useState<boolean>(false);

  React.useEffect(() => {
    (async () => {
      if (!username || user?.login === username) {
        return;
      }

      try {
        setIsLoading(true);
        setIsError(false);
        const response = await request("GET /users/{username}", { username });
        setUser(response.data);
      } catch (error) {
        console.error(error);
        setIsError(true);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [username, user?.login]);

  return [user, isLoading, isError];
}

type useGithubUserDetailsResult = ApiHookResult<GithubUser>;
