import React from "react";
import { request } from "@octokit/request";
import type { GithubRepo } from "../models";
import type { ApiHookResult } from ".";

export function useGithubUserRepositories(username: string): useGithubUserRepositoriesResult {
  const [user, setUser] = React.useState<GithubRepo[]>([]);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [isError, setIsError] = React.useState<boolean>(false);

  React.useEffect(() => {
    (async () => {
      if (!username) {
        return;
      }

      try {
        setIsLoading(true);
        setIsError(false);
        const response = await request("GET /users/{username}/repos", { username, type: "owner" });

        if(response.status === 200) {
          setUser(response.data.sort(x => x.stargazers_count));
        } else {
          throw new Error('User not found')
        }
      } catch (error) {
        console.error(error);
        setIsError(true);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [username]);

  return [user, isLoading, isError];
}

type useGithubUserRepositoriesResult = ApiHookResult<GithubRepo[]>;
