import React from "react";
import { request } from "@octokit/request";
import { GithubUser } from "../models";
import { ApiHookResult } from ".";

export function useGithubUsers(searchPhrase: string): useGithubUsersResult {
  const [users, setUsers] = React.useState<GithubUser[]>([]);
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const [isError, setIsError] = React.useState<boolean>(false);

  React.useEffect(() => {
    (async () => {
      if (!searchPhrase) {
        return [];
      }

      try {
        setIsLoading(true);
        setIsError(false);
        const response = await request("GET /search/users", { q: searchPhrase });
        setUsers(response.data.items);
      } catch (error) {
        console.error(error);
        setIsError(true);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [searchPhrase]);

  return [users, isLoading, isError];
}

type useGithubUsersResult = ApiHookResult<GithubUser[]>;
