export * from './fetch-user-details';
export * from './fetch-user-repos';
export * from './search-users';

export type ApiHookResult<T> = [T, boolean, boolean]