import css from "styled-jsx/css";

export const appStyle = css`
  .AppContainer {
    height: 100%;
  }

  .AppContainer > main {
    display: flex;
    flex-flow: column;
    padding: 0px 0.5rem;
    gap: 0.5rem;
  }

  .ScrollableContainer {
    overflow-y: auto;
    height: calc(100vh - 110px);
    padding: 0 16px;
    margin: 0px -8px;
    box-sizing: border-box;
  }
`;
