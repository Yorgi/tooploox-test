import css from "styled-jsx/css";

export const globalStyle = css.global`
  html {
    color: #030404;
    font-family: Roboto;
    font-size: 16px;
  }

  html,
  body,
  div#__next {
    height: 100%;
    margin: 0;
  }
`;
