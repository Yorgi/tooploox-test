import React from "react";
import type { GithubDetailedUser, GithubUser } from "../../data/models";

type AppState = {
  user: GithubUser;
  searchPhrase: string;
};

type AppStateAction =
  | { type: "SET_SEARCH_PHRASE"; payload: string }
  | { type: "SELECT_USER"; payload: GithubUser | GithubDetailedUser };

type Dispatch = React.Dispatch<AppStateAction>;

type AppStateStore = AppState & { dispatch: Dispatch };