import React from "react";
import type { AppState, AppStateAction } from "./types";

export function useAppStateReducer(): [AppState, React.Dispatch<AppStateAction>] {
  return React.useReducer(appStateReducer, initialState)
} 

const appStateReducer: React.Reducer<AppState, AppStateAction> = (state, action) => {
  switch(action.type) {
    case 'SET_SEARCH_PHRASE': {
      return { ...state, searchPhrase: action.payload }
    }
    case 'SELECT_USER': {
      return { ...state, user: action.payload }
    }
    default: return state;
  } 
}

const initialState: AppState = {
  searchPhrase: '',
  user: undefined
}