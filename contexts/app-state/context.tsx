import React from "react";
import { useAppStateReducer } from "./reducer";
import type { AppStateStore } from "./types";

const AppStateContext = React.createContext<AppStateStore>(null);

export const AppStateProvider: React.FunctionComponent = ({ children }) => {
  const [appState, dispatch] = useAppStateReducer();
  const contextValue = React.useMemo(() => ({ ...appState, dispatch }), [appState]);

  return (
    <AppStateContext.Provider value={contextValue}>
      {children}
    </AppStateContext.Provider>
  );
};

export function useAppState(): AppStateStore {
  return React.useContext(AppStateContext);
};


