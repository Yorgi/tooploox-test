import React from "react";
import type { NextPage } from "next";
import { List } from "../components/list";
import { useAppState } from "../contexts/app-state";
import { useGithubUsers } from "../data/hooks";
import { GithubUser } from "../data/models";
import { NoResultFound } from "../components/list/no-result";

const Home: NextPage = () => {
  const { searchPhrase } = useAppState();
  const [users, isLoading, isError] = useGithubUsers(searchPhrase);

  if (isError) {
    return <React.Fragment>Error occured</React.Fragment>;
  } else if (isLoading) {
    return <React.Fragment>Loading users...</React.Fragment>;
  }

  if (searchPhrase && users.length === 0) {
    return (
      <NoResultFound>We couldn't find anything like {searchPhrase}</NoResultFound>
    );
  }

  return (
    <List<GithubUser>
      items={users}
      displayProp="login"
      getKey={(item) => item.login}
      getHref={(item) => `details/${item.login}`}
    />
  );
};

export default Home;
