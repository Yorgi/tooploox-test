import type { NextPage } from "next";
import { useRouter } from "next/router";
import React from "react";
import { UserDetails } from "../../components/user-details";
import { UserDetailsFetcher } from "../../components/user-details/fetcher";
import { useAppState } from "../../contexts/app-state";

const UserDetailsPage: NextPage = () => {
  const router = useRouter();
  const { user } = useAppState();
  const { username } = router.query;

  const login = Array.isArray(username) ? username[0] : username;

  if (!user || user.login !== login) {

    return <UserDetailsFetcher login={login}/>
  }

  return <UserDetails user={user} />;
};

export default UserDetailsPage;