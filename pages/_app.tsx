import type { AppProps } from "next/app";
import Head from "next/head";
import { SearchBox } from "../components/searchbox";
import { AppStateProvider, useAppState } from "../contexts/app-state";
import { globalStyle, appStyle } from "../styles";

const AppWrapper: React.FunctionComponent<AppProps> = ({
  Component,
  pageProps,
}) => {
  return (
    <div className="AppContainer">
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap"
          rel="stylesheet"
        />
      </Head>

      <main>
        <AppStateProvider>
          <SearchboxWrapper />
          <div className="ScrollableContainer">
            <Component {...pageProps} />
          </div>
        </AppStateProvider>
      </main>
      <style jsx global>
        {globalStyle}
      </style>
      <style jsx>{appStyle}</style>
    </div>
  );
};

const SearchboxWrapper: React.FunctionComponent = () => {
  const { dispatch } = useAppState();
  return (
    <SearchBox
      onSearch={(value) => {
        dispatch({ type: "SELECT_USER", payload: null });
        dispatch({ type: "SET_SEARCH_PHRASE", payload: value });
      }}
    />
  );
};

export default AppWrapper;
